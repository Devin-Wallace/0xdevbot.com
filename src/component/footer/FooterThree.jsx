import React from 'react';
import {FaTwitter, FaInstagram, FaFacebookF, FaLinkedinIn, FaGitlab, FaHackerrank, FaCode} from "react-icons/fa";

const SocialShare = [
    {Social: <FaGitlab /> , link: 'https://gitlab.com/Devin-Wallace'},
    {Social: <FaLinkedinIn /> , link: 'https://www.linkedin.com/in/devin-wallace-34167998/'},
    {Social: <FaHackerrank /> , link: 'https://www.hackerrank.com/devinwallace'},
    {Social: <FaCode /> , link: 'https://gitlab.com/Devin-Wallace/personal-site'},
]

const FooterThree = () => {
    return (
        <div className="footer-style-2 ptb--30 bg_image footer-three" data-black-overlay="5">
            <div className="wrapper plr--50 plr_sm--20">
                <div className="row align-items-center justify-content-between">
                    <div className="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div className="inner">
                            <div className="logo text-center text-sm-left mb_sm--20">
                                <a href="/home-one">
                                    {/*<img src="/assets/images/logo/33006710_VIB_2.ico" alt="Logo images"/>*/}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div className="inner text-center">
                            <ul className="social-share rn-lg-size d-flex justify-content-center liststyle">
                                {SocialShare.map((val , i) => (
                                    <li key={i}><a href={`${val.link}`}>{val.Social}</a></li>
                                ))}
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-12 col-sm-12 col-12">
                        <div className="inner text-lg-right text-center mt_md--20 mt_sm--20">
                            <div className="text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default FooterThree;