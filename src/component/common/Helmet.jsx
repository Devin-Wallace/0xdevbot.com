import React, { Component } from "react";
import {Helmet} from 'react-helmet'

class PageHelmet extends Component{
    render(){
        return(
            <React.Fragment>
                <Helmet>
                    <title>Devin Wallace</title>
                    <meta name="Devin Wallace" />
                </Helmet>
            </React.Fragment>
        )
    }
}


export default PageHelmet;
