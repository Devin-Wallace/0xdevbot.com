import React, { Component } from "react";
import {Link} from "react-router-dom";

const PortfolioListContent = [
    {
        image: 'rectangle',
        category: 'Finished',
        title: 'Rectangle Checker',
        link: '/DevbotSite'
    },
    {
        image: 'discord',
        category: 'Finished',
        title: 'John Cena Discord Bot'
    },
    {
        image: 'c3',
        category: 'WIP',
        title: 'C3 Streaming Service (SaaS)',
        link: "/C3StreamingService"
    },
    // {
    //     image: 'image-4',
    //     category: 'Finished',
    //     title: ''
    // },
    // {
    //     image: 'image-3',
    //     category: 'WIP',
    //     title: 'Ok google vision'
    // },
    // {
    //     image: 'image-4',
    //     category: 'WIP',
    //     title: 'C3 Marketplace'
    // }
]

class PortfolioList extends Component{
    render(){
        const {column , styevariation } = this.props;
        const list = PortfolioListContent.slice(0 , this.props.item);
        return(
            <React.Fragment> 
                {list.map((value , index) => (
                    <div className={`${column}`} key={index}>
                        <div className={`portfolio ${styevariation}`}>
                            <div className="thumbnail-inner">
                                <div className={`thumbnail ${value.image}`}/>
                                <div className={`bg-blr-image ${value.image}`}/>
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <p>{value.category}</p>
                                    <h4><a href={value.link}>{value.title}</a></h4>
                                    <div className="portfolio-button">
                                        <a className="rn-btn" href={value.link}>View Details</a>
                                    </div>
                                </div>
                            </div>
                            <Link className="link-overlay" to={value.link}/>
                        </div>
                    </div>
                ))}
               
            </React.Fragment>
        )
    }
}
export default PortfolioList;